"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Polling {
    constructor(task) {
        this.task = task;
        if (!task) {
            throw new Error('Illegal argument Exception');
        }
        this.listeners = new Map();
    }
    setIntervar(time) {
        this.interval = time;
    }
    getInterval() {
        return this.interval;
    }
    isRunning() {
        return this.timer !== undefined && this.timer !== null;
    }
    start() {
        if (this.isRunning()) {
            return;
        }
        if (isNaN(this.interval)) {
            this.interval = 50000;
        }
        const fn = (err, result) => {
            if (err) {
                this.rise('error', err);
                return;
            }
            this.rise('success', result);
        };
        this.timer = setInterval(() => {
            this.task(fn);
        }, this.interval);
        setTimeout(() => this.task(fn), 0);
    }
    stop() {
        if (!this.isRunning()) {
            return;
        }
        clearInterval(this.timer);
        this.timer = null;
    }
    on(event, listener) {
        const listeners = this.listeners.get(event);
        const newListeners = listeners ? [...listeners, listener] : [listener];
        this.listeners.set(event, newListeners);
    }
    remove(event, listener) {
        const listeners = this.listeners.get(event);
        if (listeners) {
            const index = listeners.indexOf(listener);
            const newListeners = listeners.splice(index, 1);
            this.listeners.set(event, newListeners);
        }
    }
    rise(event, value) {
        const listeners = this.listeners.get(event);
        if (!listeners)
            return;
        listeners.forEach((fn) => {
            fn(event, value);
        });
    }
}
exports.Polling = Polling;
//# sourceMappingURL=Polling.js.map