export class Polling {

  private interval: number
  private timer: any
  private listeners: Map<string, Array<Function>>

  constructor(private task: (dispatch: (error: Error | null, result: any) => void) => void) {
    if (!task) {
      throw new Error('Illegal argument Exception')
    }
    this.listeners = new Map()
  }

  setIntervar(time: number): void {
    this.interval = time
  }
  getInterval(): number {
    return this.interval
  }

  isRunning(): boolean {
    return this.timer !== undefined && this.timer !== null
  }

  start(): void {
    if (this.isRunning()) {
      return
    }
    if (isNaN(this.interval)) {
      this.interval = 50000
    }
    const fn = (err: Error | null, result: any) => {
      if (err) {
        this.rise('error', err)
        return
      }
      this.rise('success', result)
    }
    this.timer = setInterval(() => {
      this.task(fn)
    }
    , this.interval)
    setTimeout(() => this.task(fn), 0)
  }
  stop(): void {
    if (!this.isRunning()) {
      return
    }
    clearInterval(this.timer)
    this.timer = null
  }
  on(event: string, listener: (event: string, value: any) => void) {
    const listeners = this.listeners.get(event)
    const newListeners = listeners ? [...listeners, listener] : [listener]
    this.listeners.set(event, newListeners)
  }
  remove(event: string, listener: any): void {
    const listeners = this.listeners.get(event)
    if (listeners) {
      const index = listeners.indexOf(listener)
      const newListeners = listeners.splice(index, 1)
      this.listeners.set(event, newListeners)
    }
  }
  private rise(event: string, value: any) {
    const listeners = this.listeners.get(event)
    if (!listeners) return
    listeners.forEach((fn: Function) => {
      fn(event, value)
    })
  }
}
