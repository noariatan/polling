export class Polling {

  constructor(task: (dispatch: (error: Error, result: any) => void) => void)
  setIntervar(time: number): void
  getInterval(): number
  isRunning(): boolean
  start(): void
  stop(): void
  on(event: string, listener: (event: string, value: any) => void): void
  remove(event: string, listener: any): void

}
